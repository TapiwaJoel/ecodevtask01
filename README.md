### Developer Task 1 

Tapiwa Joel Mudavanhu Assignment Response

###Instruction###

1. Change the maven-compiler-plugin executable to to match your local jdk path (javac) e.g from

```xml 
			<plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <fork>true</fork>
                    <executable>C:\Program Files\Java\jdk1.8.0_144\bin\javac</executable>
                </configuration>
            </plugin>
```

to

```xml 
		<plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <fork>true</fork>
                    <executable>your/path/to/javac</executable>
                </configuration>
            </plugin>
```

2. When testing Rest API endpoints, I Preload RequestPartner details with code 'hot-recharges'. 
Kindly use that one else you get the error 'Invalid partner code' e.g http://localhost:8880/electronic-payments-api/resources/services/enquiries/hot-recharges/balances/263775962445



