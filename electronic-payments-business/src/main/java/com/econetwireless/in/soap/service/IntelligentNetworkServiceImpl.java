
package com.econetwireless.in.soap.service;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "IntelligentNetworkServiceImpl", targetNamespace = "http://service.soap.in.econetwireless.com/", wsdlLocation = "http://localhost:8888/intelligent-network-api/IntelligentNetworkService?wsdl")
public class IntelligentNetworkServiceImpl
    extends Service
{

    private final static URL INTELLIGENTNETWORKSERVICEIMPL_WSDL_LOCATION;
    private final static WebServiceException INTELLIGENTNETWORKSERVICEIMPL_EXCEPTION;
    private final static QName INTELLIGENTNETWORKSERVICEIMPL_QNAME = new QName("http://service.soap.in.econetwireless.com/", "IntelligentNetworkServiceImpl");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8888/intelligent-network-api/IntelligentNetworkService?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        INTELLIGENTNETWORKSERVICEIMPL_WSDL_LOCATION = url;
        INTELLIGENTNETWORKSERVICEIMPL_EXCEPTION = e;
    }

    public IntelligentNetworkServiceImpl() {
        super(__getWsdlLocation(), INTELLIGENTNETWORKSERVICEIMPL_QNAME);
    }

    public IntelligentNetworkServiceImpl(WebServiceFeature... features) {
        super(__getWsdlLocation(), INTELLIGENTNETWORKSERVICEIMPL_QNAME, features);
    }

    public IntelligentNetworkServiceImpl(URL wsdlLocation) {
        super(wsdlLocation, INTELLIGENTNETWORKSERVICEIMPL_QNAME);
    }

    public IntelligentNetworkServiceImpl(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, INTELLIGENTNETWORKSERVICEIMPL_QNAME, features);
    }

    public IntelligentNetworkServiceImpl(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public IntelligentNetworkServiceImpl(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns IntelligentNetworkService
     */
    @WebEndpoint(name = "IntelligentNetworkPort")
    public IntelligentNetworkService getIntelligentNetworkPort() {
        return super.getPort(new QName("http://service.soap.in.econetwireless.com/", "IntelligentNetworkPort"), IntelligentNetworkService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns IntelligentNetworkService
     */
    @WebEndpoint(name = "IntelligentNetworkPort")
    public IntelligentNetworkService getIntelligentNetworkPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://service.soap.in.econetwireless.com/", "IntelligentNetworkPort"), IntelligentNetworkService.class, features);
    }

    private static URL __getWsdlLocation() {
        if (INTELLIGENTNETWORKSERVICEIMPL_EXCEPTION!= null) {
            throw INTELLIGENTNETWORKSERVICEIMPL_EXCEPTION;
        }
        return INTELLIGENTNETWORKSERVICEIMPL_WSDL_LOCATION;
    }

}
