package com.econetwireless.epay.business.services.api;

/**
 * Created by tnyamakura on 18/3/2017.
 */
public interface PartnerCodeValidator {
    boolean validatePartnerCode(String partnerCode);
}
