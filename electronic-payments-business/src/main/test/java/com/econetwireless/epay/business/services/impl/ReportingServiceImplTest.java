package com.econetwireless.epay.business.services.impl;

import com.econetwireless.epay.dao.subscriberrequest.api.SubscriberRequestDao;
import com.econetwireless.epay.domain.SubscriberRequest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ReportingServiceImplTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreditsServiceImpl.class);

    @Test
    public void testValidatePartnerCodeSuccess() {
        SubscriberRequest subscriberRequest = new SubscriberRequest();
        subscriberRequest.setId((long) 2);
        subscriberRequest.setRequestType("Airtime Topup");
        String partnerCode1 = "hot-recharge";
        subscriberRequest.setPartnerCode(partnerCode1);
        subscriberRequest.setMsisdn("263774222193");
        subscriberRequest.setBalanceBefore(0.39);
        subscriberRequest.setBalanceAfter(2.41);
        subscriberRequest.setAmount(0.00);
        subscriberRequest.setDateCreated(new Date());
        subscriberRequest.setDateLastUpdated(new Date());
        subscriberRequest.setStatus("Successful");
        subscriberRequest.setReference("TOPUP-REF-01");
        subscriberRequest.setVersion((long) 2.02);

        List<SubscriberRequest> subscriberRequestList = new ArrayList<>();
        subscriberRequestList.add(subscriberRequest);

        SubscriberRequestDao subscriberRequestDao = mock(SubscriberRequestDao.class);
        String partnerCode = "hot-recharge";
        when(subscriberRequestDao.findByPartnerCode(partnerCode)).thenReturn(subscriberRequestList);

        ReportingServiceImpl reportingService = new ReportingServiceImpl(subscriberRequestDao);
        List<SubscriberRequest> subscriberRequestsByPartnerCode = reportingService.findSubscriberRequestsByPartnerCode(partnerCode);
        LOGGER.info("subscriberRequestsByPartnerCode: {}", subscriberRequestsByPartnerCode);
        assertTrue(1 <= subscriberRequestsByPartnerCode.size());
    }
}