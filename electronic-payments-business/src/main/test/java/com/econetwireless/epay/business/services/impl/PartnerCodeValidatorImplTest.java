package com.econetwireless.epay.business.services.impl;

import com.econetwireless.epay.dao.requestpartner.api.RequestPartnerDao;
import com.econetwireless.epay.domain.RequestPartner;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PartnerCodeValidatorImplTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreditsServiceImpl.class);

    @Test
    public void testValidatePartnerCodeSuccess() {
        RequestPartner requestPartner = new RequestPartner();
        requestPartner.setDescription("HOT RECHARGE");
        requestPartner.setName("Hot recharge");
        requestPartner.setCode("hot-recharge");
        requestPartner.setId((long) 1);
        RequestPartnerDao requestPartnerDao = mock(RequestPartnerDao.class);
        String partnerCode = "hot-recharge";
        when(requestPartnerDao.findByCode(partnerCode)).thenReturn(requestPartner);

        PartnerCodeValidatorImpl partnerCodeValidatorImpl = new PartnerCodeValidatorImpl(requestPartnerDao);
        boolean validatePartnerCode = partnerCodeValidatorImpl.validatePartnerCode(partnerCode);
        LOGGER.info("partnerCodeValidatorImpl: {}", validatePartnerCode);
        assertTrue(validatePartnerCode);
    }

    @Test(expected = Exception.class)
    public void testValidatePartnerCodeFail() {
        RequestPartner requestPartner = null;
        RequestPartnerDao requestPartnerDao = mock(RequestPartnerDao.class);
        String partnerCode = "hot-recharge";
        when(requestPartnerDao.findByCode(partnerCode)).thenReturn(requestPartner);
        PartnerCodeValidatorImpl partnerCodeValidatorImpl = new PartnerCodeValidatorImpl(requestPartnerDao);
        partnerCodeValidatorImpl.validatePartnerCode(partnerCode);
    }
}