package com.econetwireless.epay.business.services.impl;

import com.econetwireless.epay.business.integrations.api.ChargingPlatform;
import com.econetwireless.epay.dao.requestpartner.api.RequestPartnerDao;
import com.econetwireless.epay.dao.subscriberrequest.api.SubscriberRequestDao;
import com.econetwireless.epay.domain.SubscriberRequest;
import com.econetwireless.utils.messages.AirtimeTopupRequest;
import com.econetwireless.utils.messages.AirtimeTopupResponse;
import com.econetwireless.utils.pojo.INCreditRequest;
import com.econetwireless.utils.pojo.INCreditResponse;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CreditsServiceImplTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreditsServiceImplTest.class);


    @Test
    public void testCreditMethod() {
        String partnerCode = "hot-recharge";
        SubscriberRequestDao subscriberRequestDao = mock(SubscriberRequestDao.class);
        ChargingPlatform chargingPlatform = mock(ChargingPlatform.class);
        CreditsServiceImpl creditsService = new CreditsServiceImpl(chargingPlatform, subscriberRequestDao);


        final AirtimeTopupRequest airtimeTopupRequest = new AirtimeTopupRequest();
        airtimeTopupRequest.setPartnerCode(partnerCode);
        airtimeTopupRequest.setReferenceNumber("TOPUP-REF-0123");
        airtimeTopupRequest.setAmount(1.11);
        airtimeTopupRequest.setMsisdn("772984803");


        SubscriberRequest subscriberRequest = new SubscriberRequest();
        subscriberRequest.setId((long) 2);
        subscriberRequest.setRequestType("Airtime Topup");
        subscriberRequest.setPartnerCode(partnerCode);
        subscriberRequest.setMsisdn("263774222193");
        subscriberRequest.setBalanceBefore(0.39);
        subscriberRequest.setBalanceAfter(2.41);
        subscriberRequest.setAmount(0.00);
        subscriberRequest.setDateCreated(new Date());
        subscriberRequest.setDateLastUpdated(new Date());
        subscriberRequest.setStatus("Successful");
        subscriberRequest.setReference("TOPUP-REF-01");
        subscriberRequest.setVersion((long) 2.02);

        INCreditRequest inCreditRequest = new INCreditRequest();
        inCreditRequest.setAmount(200);
        inCreditRequest.setMsisdn("263774222193");
        inCreditRequest.setPartnerCode(partnerCode);
        inCreditRequest.setReferenceNumber("TOPUP-REF-01");

        INCreditResponse inCreditResponse = new INCreditResponse();
        inCreditResponse.setBalance(300);
        inCreditResponse.setMsisdn("263774222193");
        inCreditResponse.setNarrative("Credit Success");
        inCreditResponse.setResponseCode("200");

        List<SubscriberRequest> subscriberRequestList = new ArrayList<>();
        subscriberRequestList.add(subscriberRequest);
        when(subscriberRequestDao.save(any(SubscriberRequest.class))).thenReturn(subscriberRequest);
        when(subscriberRequestDao.save(any(SubscriberRequest.class))).thenReturn(subscriberRequest);
        when(chargingPlatform.creditSubscriberAccount(any(INCreditRequest.class))).thenReturn(inCreditResponse);
        AirtimeTopupResponse credit = creditsService.credit(airtimeTopupRequest);
        LOGGER.info("creditsService.credit(airtimeTopupRequest): {}", credit);
        assertNotNull(credit);
    }
}