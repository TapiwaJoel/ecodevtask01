-- noinspection SqlNoDataSourceInspectionForFile
-- drop table e_req_partner;
-- drop table e_request;
CREATE TABLE e_req_partner (
  id         INTEGER PRIMARY KEY,
  code VARCHAR(30),
  name VARCHAR(50),
  description VARCHAR(50)
);

CREATE TABLE e_request (
  id          BIGINT IDENTITY PRIMARY KEY,
  request_type VARCHAR(30),
  partner_code VARCHAR(50),
  mobile_number VARCHAR(8),
  balance_before double,
  balance_after double,
    amount double,
  creation_date TIMESTAMP,
  date_updated TIMESTAMP,
  status VARCHAR(30),
  reference_number VARCHAR(30),
  version BIGINT
);

-- delete from e_req_partner  where id = 124;
delete from e_req_partner;
INSERT INTO e_req_partner (id, code, name, description)
VALUES (124, 'hot-recharges', 'Hot Recharge', 'HOT RECHARGE PLATFORM');
-- delete from e_req_partner where id in (1,10);
delete from e_request;
INSERT INTO e_request (id, request_type, partner_code, mobile_number, balance_before, balance_after, amount, creation_date, date_updated, status, reference_number, version)
VALUES (10,'Airtime Balance Enquiry', 'hot-recharges', '263774222382', 20.76, 20.76, 0.00, '2017-03-18 11:52:03', '2017-03-18 11:52:03', 'Successful', 'BAL-REF-01', 0);
INSERT INTO e_request (id, request_type, partner_code, mobile_number, balance_before, balance_after, amount, creation_date, date_updated, status, reference_number, version)
VALUES (1,'Airtime Topup', 'hot-recharges', '263774222193', 0.39, 2.41, 0.00, '2017-03-18 11:52:05', '2017-03-18 11:52:07', 'Successful', 'TOPUP-REF-01', 2.02);